const root = this;
return require('src/data.js')
  .then(reqs => {
    var data = reqs['src/data.js'];
    class Match {
      static get insensitive() {
        return Object.getOwnPropertyNames(Match)
                     .filter(n => n==='insensitive'||typeof(Match[n])==='function')
                     .reduce((m,n)=>(m[n]=function(){
                       return Match[n].apply(Match, Array.from(arguments)
                         .map(a => typeof(a)==='string'?a.toLowerCase():a)
                       )
                     })&&m,{});
      }

      static partial(a,b) {
        return (a||'').toString().replace(/\s+/g,'').includes((b||'').toString().replace(/\s+/g,''));
      }

      static exact(a,b) {
        return (a||'').toString().replace(/\s+/g,'') == (b||'').toString().replace(/\s+/g,'');
      }
    }

    class UX { // Common experience / functionality items

      static get match() { return Match; }

      static createPageLink(url, text) {
        var link = document.createElement('a');
        link.setAttribute('target', '_blank');
        link.setAttribute('href', url);
        link.textContent = text;
        return link;
      }

      static createItemLink(path, text) {
        return UX.createPageLink(data.environmentRootUrl + path, text);
      }

      static createEngagementLink(engagement, type, text) {
        var path = 'engagements/' + (type===1?'view/':type===2?'duplicate/':'edit/');
        var link = UX.createItemLink(path + engagement.id, text || engagement.name);
        link.classList.add(engagement.engagementEnabled?'engagement-active':'engagement-inactive');
        return link;
      }

      static createEngagementEditLink(engagement, text) {
        return UX.createEngagementLink(engagement,0,text);
      }

      static createEngagementViewLink(engagement, text) {
        return UX.createEngagementLink(engagement,1,text);
      }

      static createEngagementDupeLink(engagement, text) {
        return UX.createEngagementLink(engagement,2,text);
      }

      static createActionLink(action) {
        return UX.createItemLink('engagements/actions/edit/' + action.id, action.name);
      }

      static createSegmentLink(segment) {
        var path = (segment.type||'').toLowerCase() === 'targetlist' ? 'segmentation/targetlist/' : 'new-segmentation/edit/';
        return UX.createItemLink(path + segment.id, segment.name);
      }

      static search(query) {
        console.log('searching...');
        var partial = Match.insensitive.partial;
        var exact = Match.insensitive.exact;
        var search = (query || '').trim()
          .split(/\s+/g).filter(s => s)
          .map(term => { return {
            term: term.replace(/^[\-\!\~\^\?]/,''),
            found: term.match(/^\-/) ? false : true,
            compare: term.match(/^\!/) ? Match.exact :
                     term.match(/^\~/) ? Match.insensitive.exact :
                     term.match(/^\^/) ? Match.partial :
                     term.match(/^\?/) ? Match.insensitive.partial :
                     false
          };});
        if (!search.length) { return false; }

        return {
          engagements: data.engagements.filter(eng => search.every(s => {
            if ((s.compare||partial)(eng.name, s.term)) { return s.found; }
            if ((s.compare||partial)(eng.description, s.term)) { return s.found; }
            if ((s.compare||exact)(eng.id, s.term)) { return s.found; }

            var refs = eng.json&&eng.json.decisionPointRefinements;
            if (refs && refs.length) {
              if (refs.some(r =>
                (s.compare||partial)(r.value, s.term)
                || (s.compare||partial)(r.jsonKey&&r.jsonKey.name, s.term)
              )) { return s.found; }
              if (refs.some(r =>
                r.jsonKey&&r.jsonKey.tags&&r.jsonKey.tags.some(t =>
                  (s.compare||exact)(t, s.term)
                )
              )) { return s.found; }
              if (refs.some(r =>
                r.value&&(s.compare||partial)(r.value, s.term)
              )) { return s.found; }
            }

            if (eng.json&&eng.json.details&&eng.json.details.decisionPoint) {
              if ((s.compare||exact)(eng.json.details.decisionPoint.name, s.term)) { return s.found; }
            }

            if (eng.tagIds) {
              if (eng.tagIds.some(t => (s.compare||exact)(t, s.term))) { return s.found; }
            }

            if (eng.qualityIds) {
              if (eng.qualityIds.some(q => (s.compare||exact)(q,s.term))) { return s.found; }
            }

            return !s.found;
          })),

          segments: data.segments.filter(seg => search.every(s => {
            if ((s.compare||partial)(seg.name, s.term)) { return s.found; }
            if ((s.compare||partial)(seg.description, s.term)) { return s.found; }
            if ((s.compare||partial)(seg.sql, s.term)) { return s.found; }
            if ((s.compare||exact)(seg.id, s.term)) { return s.found; }
            if (seg.criteria && seg.criteria.length) {
              if(seg.criteria.some(c => (s.compare||partial)(c.value, s.term))) { return s.found; }
            }
            if (seg.queryConditionGroups&&seg.queryConditionGroups.length) {
              if (seg.queryConditionGroups.some(q => {
                if (q.conditions&&q.conditions.length) {
                  if (q.conditions.some(c => (s.compare||partial)(c.value, s.term))) { return s.found; }
                }
              })) { return s.found; }
            }
            return !s.found;
          })),

          actions: data.actions.filter(act => search.every(s => {
            if ((s.compare||partial)(act.name, s.term)) { return s.found; }
            if ((s.compare||partial)(act.description, s.term)) { return s.found; }
            if ((s.compare||exact)(act.id, s.term)) { return s.found; }
            var parms = act.json&&act.json.parameters;
            if (parms&&parms.length) {
              if (parms.some(p => (s.compare||partial)(p.value, s.term))) { return s.found; }
            }
            return !s.found;
          }))
        }
      }
    }
    root.ux = UX;
  }).then(() => root.ux);
