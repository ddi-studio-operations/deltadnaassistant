const root = this;
root.modules = [];

var tabs = Array.from(root.querySelectorAll('module-tabs'));
var mods = Array.from(root.querySelectorAll('ddna-module'));

return require('io')
  .then(reqs => {
    var io = reqs['io'];

    return Promise.all(
      mods.map(e => io.local(e.getAttribute('src'))
          .then(c => { return {
            name: e.getAttribute('name'),
            element: e,
            content: c
          }})
      )
    ).then(modules => Promise.all(
      modules.map(module => {
        root.modules.push(module);
        module.select = () => {
          modules.forEach(m => m.tabs.forEach(t => t.classList.remove('active')));
          module.tabs.forEach(t => t.classList.add('active'));
          modules.forEach(m => m.element.classList.remove('active'));
          module.element.classList.add('active');
        };

        module.tabs = tabs.map(container => {
          var tab = container.appendChild(document.createElement('ddna-tab'));
          tab.textContent = module.name;
          tab.addEventListener('click', module.select);
          return tab;
        });

        var doc = io.parseFragment(module.content, 'Module: ' + module.name);
        module.element.appendChild(doc.content);
        return doc.execute(module.element, {
          extension: root,
          module: module.element,
          tabs: module.tabs,
        });
      })
    ));
  })
  .then(() => root.modules[0].select())
  .then(() => root.modules);
