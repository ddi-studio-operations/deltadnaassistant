const root = this;
root.data = {};

function sortEngagements(e) {
  return e.sort((a,b) => {
    if (a.engagementEnabled > b.engagementEnabled) { return -1; }
    if (a.engagementEnabled < b.engagementEnabled) { return 1; }
    if (a.name > b.name) { return 1; }
    if (a.name < b.name) { return -1; }
    return 0;
  });
}

function sortActions(a) {
  return a.sort((a,b) => {
    if (a.name > b.name) { return -1; }
    if (a.name < b.name) { return 1; }
    return 0;
  });
}

function sortSegments(s) {
  return s.sort((a,b) => {
    if (a.name > b.name) { return -1; }
    if (a.name < b.name) { return 1; }
    return 0;
  });
}

function makeArray(value) {
  return Array.isArray(value)?value:(value==null?[]:[value]);
}

return require('io')
  .then(reqs => {
    var io = reqs['io'];

    // Loading data from page
    [{required: true, items: {
      application: 'init_data_APPLICATION',
      environment: 'init_data_ENVIRONMENT'
    }},
    {required: false, items: {
      decisionPoints: 'init_data_DECISION_POINTS',
      tags: 'init_data_APPLICATION_TAGS',
      traits: 'init_data_ENGAGEMENT_QUALITY_IDS',
      keys: 'init_data_JSONKEYS',
      metrics: 'init_data_METRICS_MAP'
    }}].forEach(bootGroup => {
      for (var item in bootGroup.items) {
        var element = document.getElementById(bootGroup.items[item]);
        if (bootGroup.required&&!element) { throw new Error('Required data-item ' + item + ' unavailable'); }
        else if (!element) { console.warn('Optional data-item ' + item + ' unavailable'); }
        try { root.data[item] = JSON.parse(element.textContent); }
        catch(ex) {
          if (bootGroup.required) { throw new Error('Parsing data-item ' + item + ' failed'); }
          else { console.warn('Parsing data-item ' + item + ' failed'); }
        }
      }
    });

    // DDNA root environment URL for resources
    root.data.environmentRootUrl =
      'https://www.deltadna.net/'
      + root.data.application.accountSlug + '/'
      + root.data.application.slug + '/'
      + root.data.environment.slug + '/';

    // Downloading data not already loaded into the page. This will take some time.
    // Note: Some of these calls will be made by the page but because of the sheer volume of data they return it breaks Chrome's hooks that would allow me to tap into the existing calls
    return Promise.all([
      io.download('https://www.deltadna.net/api/engagement/engagements/list/' + root.data.environment.id),
      io.download('https://www.deltadna.net/api/engagement/actions/list/' + root.data.environment.id),
      io.download('https://www.deltadna.net/api/selections/list/' + root.data.environment.id),
    ]).then(json => {
      root.data.engagements = JSON.parse(json[0]);
      root.data.actions = JSON.parse(json[1]);
      root.data.segments = JSON.parse(json[2]);

      sortEngagements(root.data.engagements);
      sortActions(root.data.actions);
      sortSegments(root.data.segments);

      // Stitching the data together
      var dpMap = root.data.decisionPoints&&root.data.decisionPoints.reduce((m,v) => (m[v.id]=v)&&m, {});
      var tagMap = root.data.tags&&root.data.tags.reduce((m,v) => (m[v.id]=v.name)&&m, {});
      var traitMap = root.data.traits&&root.data.traits.reduce((m,v) => (m[v.id]=v.name)&&m, {});
      var metMap = root.data.metrics&&Object.keys(root.data.metrics).reduce((m,k) => m.concat(root.data.metrics[k]), []).reduce((m,v) => (m[v.id]=v)&&m,{});
      var keyMap = root.data.keys&&root.data.keys.reduce((m,v) => (m[v.id]=v.name)&&m, {});
      var sgMap = root.data.segments.reduce((m,v) => (m[v.type.toLowerCase() + '-' + v.id]=v)&&m, {});
      var actMap = root.data.actions.reduce((m,v) => (m[v.id]=v)&&m, {});

      root.data.engagements.forEach(e => {
        if (dpMap&&e.json&&e.json.details&&e.json.details.decisionPoint) {
          e.json.details.decisionPoint = dpMap[e.json.details.decisionPoint];
        }
        if (e.tagIds) {
          e.tagIds = makeArray(e.tagIds).map(k => tagMap&&tagMap[k]||k);
        }
        if (e.qualityIds) {
          e.qualityIds = makeArray(e.qualityIds).map(k => traitMap&&traitMap[k]||k);
        }
        if (e.json&&e.json.details&&e.json.details.segment) {
          e.json.details.segment = sgMap[
            e.json.details.segment.replace(/[^a-z0-9\-]/gi,'').toLowerCase()
          ];
        }
        if (e.json&&e.json.actions) {
          e.json.actions = makeArray(e.json.actions)
            .filter(a => a&&a.action)
            .map(a => actMap[a.action])
            .filter(a => a);
        }
        if (e.json&&e.json.decisionPointRefinements) {
          (e.json.decisionPointRefinements=makeArray(e.json.decisionPointRefinements))
            .forEach(ref => ref.tags = makeArray(ref.tags)
              .map(id => tagMap&&tagMap[id]||id)
              .filter(t => t)
            );
        }
        if (e.actionJson) {
          e.actionJson = makeArray(e.actionJson)
            .filter(a => a&&a.id)
            .map(a => actMap[a.id])
            .filter(a => a);
        }
      });

      root.data.actions.forEach(a => {
        if (a.json&&a.json.parameters) {
          (a.json.parameters = makeArray(a.json.parameters))
            .filter(a => a&&a.key)
            .forEach(a => a.key = keyMap&&keyMap[a.key]||a.key);
        }
        if (a.tagIds) {
          a.tagIds = makeArray(a.tagIds)
            .map(t => tagMap&&tagMap[t]||t)
            .filter(t => t);
        }
      });

      root.data.segments.forEach(s => {
        if (s.criteria) {
          (s.criteria = makeArray(s.criteria))
            .forEach(c => c.metric = c.metricId&&metMap&&metMap[c.metricId]||null);
        }
      });

      if (root.data.decisionPoints) {
        root.data.decisionPoints.forEach(d => {
          if (d.jsonKeys) {
            (d.jsonKeys=makeArray(d.jsonKeys))
              .forEach(jk => jk.tags = makeArray(jk.tags)
                .map(id => tagMap&&tagMap[id]||id)
                .filter(t => t)
              );
          }
        });
      }

      if (root.data.keys) {
        root.data.keys.forEach(k => {
          if (k.tags) {
            k.tags = makeArray(k.tags)
              .map(id => tagMap&&tagMap[id]||id)
              .filter(t => t);
          }
        });
      }
    });
  }).then(() => root.data);
