(function() {
  console.log('Starting DeltaDnaAssistant...');

  // Entry point for extension
  var existing = document.querySelector('chrome-extension');
  if (existing) { throw 'DeltaDnaAssistant appears to be loaded more than once. Make sure to remove old versions after upgrading!'; }
  var root = document.createElement('chrome-extension');

  // Setting up a simple require/exports thing. Though extensions have native ES6 module support getting around CORS limitations proved to be tedious
  var resolutions = {};
  class IO {
    static ajax(method, url, content) {
      return new Promise((resolve, reject) => {
        var request = new XMLHttpRequest();
        request.open(method, url, true);
        request.onreadystatechange = () => {
          if (request.readyState !== XMLHttpRequest.DONE) { return; }
          if (request.status < 200 || request.status >= 300) {
            reject(request.responseText);
          } else {
            resolve(request.responseText);
          }
        };
        request.send(content);
      });
    }

    static download(url) { // Download anything
      return IO.ajax('GET', url);
    }

    static local(url) { // Download local extension files
      return IO.download(chrome.runtime.getURL(url));
    }

    static require(urls) { // Loads/caches a local dependency
      urls = Array.from(arguments)
        .flatMap(a => Array.isArray(a)?a:[a]) // accept arrays of URLs or just multiple url parameters
        .filter(a => typeof(a) === 'string' && a);
      return Promise.all(
        urls.map(url => (url
          .toLowerCase() // url normalization
          .split('?',1)[0]||'')
          .replace(/^[\s\\\/\.]+|[\s\\\/\.]+$/g, '')
        ).map(url => resolutions[url]||( // Cache under normalized url
          resolutions[url]=IO.local(url+'?t=' + Math.random())
            .then(js => { // Execute
              try { return IO.resolve('console.log("Loading: ' + url + '"); ' + js, root); }
              catch(ex) { console.error(url, ex); throw ex; }
            })
        ))
      ).then(reqs => reqs.forEach((v,i) => reqs[urls[i]] = v)||reqs);
    }

    static resolve(js, context, env) { // Evals JS in a semi-isolated scope with customizable environment globals
      return Promise.resolve( // If the JS returns a promise this will automatically become an async call
        new Function(...['require'].concat(Object.keys(env||{})).concat([js]))
           .apply(context, [function(){return IO.require(...arguments)}].concat(Object.values(env||{})))
      );
    }

    static parseFragment(html, note) { // parses HTML and returns a content fragment and execution trigger
      var dom = new DOMParser().parseFromString(html, 'text/html');
      var scripts = Array.from(dom.querySelectorAll('script')).map(s => s.parentElement.removeChild(s));
      var fragment = document.createDocumentFragment();
      while (dom.head.firstChild) { fragment.appendChild(dom.head.firstChild); }
      while (dom.body.firstChild) { fragment.appendChild(dom.body.firstChild); }
      return { content: fragment, execute: (context, env) => Promise.all(
        scripts.map(element => element.hasAttribute('src')
           ? IO.require(element.getAttribute('src'))
           : IO.resolve('console.log("' + (note||'->') + '"); ' + element.innerHTML, context, env)
         )
      )};
    }
  }

  resolutions.io = Promise.resolve(root.io = IO);
  IO.local('index.html')
    .then(html => {
      var doc = IO.parseFragment(html, 'Loading: index.html');
      root.appendChild(doc.content);
      document.body.appendChild(root);
      doc.execute(root)
         .then(() => {
           console.log('DeltaDnaAssistant loaded');
           root.dispatchEvent(new CustomEvent('ready'));
         })
         .catch(e => {
           console.log('DeltaDnaAssistant failed', e);
           root.dispatchEvent(new CustomEvent('fail'));
         });
    });
})();
